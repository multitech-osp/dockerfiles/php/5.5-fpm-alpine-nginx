FROM php:5.5-fpm-alpine

# Install prerequisites required for tools and extensions installed later on.
RUN apk add --update bash gnupg libpng-dev su-exec unzip zip supervisor nginx qt-dev libressl-dev

# Retrieve the script used to install PHP extensions from the source container.
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/install-php-extensions

# Install required PHP extensions and all their prerequisites available via apt.
RUN chmod uga+x /usr/bin/install-php-extensions \
    && sync \
    && install-php-extensions bcmath exif gd intl opcache pcntl redis mysqli mssql mysql pdo_mysql ldap soap imap

# Downloading composer and marking it as executable.
RUN curl -o /usr/local/bin/composer https://getcomposer.org/composer-stable.phar \
    && chmod +x /usr/local/bin/composer

# Remove HTML folder inside /var/www
RUN rm -rf /var/www/html
RUN mkdir /var/www/public
COPY ./index.php /var/www/public/index.php

# Setting the work directory.
WORKDIR /var/www

# COPY NGINX CONFIG
COPY ./nginx/mime.types /etc/nginx/mime.types
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf

# COPY PHP CONFIGS
COPY ./php/php.ini /usr/local/etc/php/php.ini
COPY ./php/php-fpm.conf /usr/local/etc/php-fpm.conf
COPY ./php/www.conf /usr/local/etc/php-fpm.d/www.conf

# COPY CRONTAB CONFIGS
COPY ./crontabs/cron /etc/cron
RUN chmod 0644 /etc/cron && crontab /etc/cron

# COPY SUPERVISOR CONFIG
COPY ./supervisor/supervisord.conf /etc/supervisord.conf

# COPY SHELLSCRIPT
COPY ./scripts/init.sh /scripts/init.sh
RUN chmod +x /scripts/init.sh

EXPOSE 80